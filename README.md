# Final Project JCC kelompok 4

Jabar Coding Camp Final Project | VueJS | Group 4

## Members

- Andre Natanael (@Andrenatanael23)
- Ardhi Sasongko (@Ardhi_Sasongko)
- Sonya Putri Utami (@Sonyaputri1)

## Scoring


- [ ] Website Functionality | 50%	
	- Authentication (Login, Register, Logout)
	- Blog CRUD (Create, Update, Delete, Upload File)
	- Navigation (Route)
- [ ] Website Deployment (Netlify) | 10%
	- Link : https://four-future.netlify.app/
- [ ] Website Aesthetic | 10%
- [ ] Clean Code | 10%
- [ ] Website Demo | 20%
	- Link :
- [ ] Screenshot
	- Link : https://drive.google.com/drive/folders/17l4V13ZbEgaJpWnX9i0jfOQrRzjgl1Oq?usp=sharing
